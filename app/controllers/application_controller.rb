class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception



  # Rails 4 moved the parameter sanitization from the model to the controller,
  # causing Devise to handle this concern at the controller as well.
  before_action :configure_permitted_parameters, if: :devise_controller?

  before_action :set_sidebar_variables

  rescue_from CanCan::AccessDenied do |e|
    redirect_to root_url, alert: e.message
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << :subdomain
  end

  def set_sidebar_variables
    # Only set sidebar variables if user has signed in
    if user_signed_in?
      @event = current_user.event
      @groom = User.with_role(:groom, @event).first
      @bride = User.with_role(:bride, @event).first
    end
  end

end
