class StoriesController < ApplicationController
    layout 'dashboard'

    # This method also load the instance variables on actions like CREATE.
    # Having the instance variable defined twice, by the method and manually
    # caused uploads not to work
    load_and_authorize_resource

    def index
        @event = Event.find(params[:event_id])
        @stories = Story.order(date: :asc)
        @story = Story.new
    end

    def destroy
        Story.find(params[:id]).destroy
        respond_to do |format|
            format.html { redirect_to event_stories_url(params[:event_id]),
                success: 'Event was successfully destroyed.' }
            format.json { head :no_content }
        end
    end

    # POST /events
    # POST /events.json
    def create
        respond_to do |format|
          if @story.save
            format.html { redirect_to event_stories_path(params[:event_id]),
                flash: { success: t('forms.stories.success') } }
            format.json { render :index, status: :created, location: @story }
          else
            @stories = Story.order(date: :asc)
            format.html { render :index }
            format.json { render json: @event.errors,
                status: :unprocessable_entity }
          end
        end
    end

    private
    def story_params
      params.require(:story).permit(:name, :description, :date, :photo,
                                    :photo_id)
    end

end
