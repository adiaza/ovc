class UsersController < ApplicationController
  load_and_authorize_resource

  layout 'dashboard'

  def show
    @user = User.find(params[:id])
    unless @user.is_bride_of?(@user.event) || @user.is_groom_of?(@user.event)
      redirect_to dashboard_path
    end
  end

  def new
    @role = params[:role]
    if ['groom', 'bride'].include?(@role)
      @user = User.new
    else
      redirect_to dashboard_path
    end
  end

  def edit

  end

  def create

    @user = User.new(user_params)

    @user.password = '12'
    @user.add_role params[:user][:role], current_user.event
    @user.event = current_user.event
    @user.skip_confirmation!

    if(params[:user][:has_partner] == "true")
      p = @user.build_partner(name: params[:user][:partner_name],
                              email: params[:user][:partner_email],
                              menu: params[:user][:partner_menu])
      p.password = '12'
      p.add_role params[:user][:role], current_user.event
      p.add_role "partner", current_user.event
      p.event = current_user.event
      p.skip_confirmation!

    end
    respond_to do |format|
      if @user.save
        # render json: @user
        # format.json { render :index, status: :created, location: @story }
        format.json {
          render :json => {
            user: @user,
            partner: @user.partner
          }
        }
      else
        render json: @user.errors, status: :unprocessable_entity
      end
    end

  end


  def destroy
    @user = User.find(params[:id])
    @user.destroy
    head :no_content
  end

  def update
    if(@user.is_guest_of?(@user.event))
      @user.skip_reconfirmation!
    end

    respond_to  do |format|
      if @user.update(user_params)
        format.html { redirect_to @user,
          flash: { success: t('forms.users.edit_ok') } }
        format.json { render json: @user, status: :ok }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def user_params
    if params[:user][:role] == 'guest'
      params.require(:user).permit(:name, :email, :menu, :confirmation_status)
    elsif (['groom', 'bride'].include?(params[:user][:role]) ||  params[:user][:role] == 'couple')
      params.require(:user).permit(:first_name, :profile_image, :last_name, :event_id, :subdomain,
      :biography, :facebook, :twitter, :instagram, :linkedin, :pinterest)
    end
  end

  def couple_params
    params.require(:user).permit(:first_name, :profile_image, :last_name, :event_id, :subdomain,
      :biography, :facebook, :twitter, :instagram, :linkedin, :pinterest)
  end

end
