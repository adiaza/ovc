class LocationsController < ApplicationController
  load_and_authorize_resource

  before_action :set_location, only: [:show, :edit, :update, :destroy]

  layout 'dashboard'

  # GET /location
  # GET /location.json
  def index
    event = Event.find(params[:event_id])
    @location = event.location

    if @location.nil?
      redirect_to new_event_location_url
    end

  end

  # GET /location/1
  # GET /location/1.json
  def show
  end

  # GET /location/new
  def new
    @event = Event.find(params[:event_id])
    @location = Location.new

    geo_location = request.location

    if geo_location.latitude == 0.0 && geo_location.longitude == 0.0
      @location.latitude = -33.447487
      @location.longitude = -70.673676
    else
      @location.latitude =  geo_location.latitude
      @location.longitude = geo_location.longitude
    end

  end

  # GET /location/1/edit
  def edit
  end

  # POST /location
  # POST /location.json
  def create
    # Commented out as it's loaded by load_and_authorize_resource
    # @location = Location.new(location_params)
    @location.event_id = params[:event_id]
    respond_to do |format|
      if @location.save
        # binding.pry
        format.html { redirect_to event_location_path(@location.event, @location), notice: 'Location was successfully created.' }
        format.json { render :show, status: :created, location: @location }
      else
        format.html { render :new }
        format.json { render json: @location.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /location/1
  # PATCH/PUT /location/1.json
  def update
    respond_to do |format|
      if @location.update(location_params)
        format.html {
            redirect_to event_location_path(@location.event, @location),
            flash: { success: t('forms.locations.success') } }
        format.json { render :show, status: :ok, location: @location }
      else
        format.html { render :edit }
        format.json { render json: @location.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /location/1
  # DELETE /location/1.json
  def destroy
    @location.destroy
    respond_to do |format|
      format.html { redirect_to dashboard_url, notice: 'Location was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_location
      @location = Location.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def location_params
      params.require(:location).permit(:address, :description, :location_image,
                                       :map_image, :event_id, :event, :name, :latitude,
                                       :longitude, :church_name, :church_address,
                                       :church_longitude, :church_latitude, :howto )
    end
end
