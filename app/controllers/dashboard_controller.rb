class DashboardController < ApplicationController

  # Authenticate: Required to use devise helpers
  before_action :authenticate_user!



  def index
    # unless current_user.has_role? :admin
    #   flash[:error] = ""
    # end
    authorize! :index, :dashboard

    unless current_user.event_id.nil?
      @event = Event.find_by_id(current_user.event_id)
      # @guests = @event.users.with_role(:guest, @event)
      @guests = @event.users.with_role(:guest, @event).includes(:partner).as_json(include: { partner: { only: [:name] } })

    else
      @event = Event.new
    end

    @guest_user = User.new

  end


end
