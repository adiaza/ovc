class GiftsController < ApplicationController
  load_and_authorize_resource

  layout 'dashboard'

  def edit
    event = Event.find(params[:event_id])
    @gift = event.gift
  end

  def new
  end

  def destroy
    Event.find(params[:event_id]).gift.destroy
    respond_to do |format|
      format.html { redirect_to new_event_gift_url(params[:event_id]),
          success: 'Event was successfully destroyed.' }
      format.json { head :no_content }
    end
    end

  def update
    event = Event.find(params[:event_id])
    @gift = event.gift

    respond_to do |format|
      if @gift.update(gift_params)
        format.html { redirect_to event_gift_path(@gift.event), notice: 'Location was successfully updated.' }
        format.json { render :show, status: :ok, location: @gift }
      else
        format.html { render :edit }
        format.json { render json: @gift.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
    event = Event.find(params[:event_id])
    @gift = event.gift
  end

  def create
    @gift.event_id = params[:event_id]
    respond_to do |format|
      if @gift.save
        # binding.pry
        format.html { redirect_to event_gift_path(@gift.event), notice: 'Gift was successfully created.' }
        format.json { render :show, status: :created, gift: @gift }
      else
        format.html { render :new }
        format.json { render json: @gift.errors, status: :unprocessable_entity }
      end

    end
  end

  private
  def gift_params
      params.require(:gift).permit(:code, :store_id, :description)
  end

end
