class RegistrationsController < Devise::RegistrationsController

  def create
    # Create Tenant after registering
    create_tenant(params[:user][:subdomain])

    # Save new user and add admin role
    super do
      resource.add_role(:admin)
      resource.create_account
      resource.save
    end

  end

  protected

  # It redirects to subdomain after a succesful signup
  def after_sign_up_path_for(resource)
    new_user_session_url(subdomain: resource.subdomain)
  end

  def create_tenant(subdomain)
    Apartment::Tenant.create(subdomain)
    Apartment::Tenant.switch!(subdomain)
  end

end