json.array!(@events) do |event|
  json.extract! event, :id, :date, :time, :description, :address, :address_description, :latitude, :longitude
  json.url event_url(event, format: :json)
end
