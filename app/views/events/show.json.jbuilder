json.extract! @event, :id, :date, :time, :description, :address, :address_description, :latitude, :longitude, :created_at, :updated_at
