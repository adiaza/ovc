json.array!(@location) do |location|
  json.extract! location, :id, :address, :description, :location_image_id, :map_image_id, :event_id, :name, :lat, :long
  json.url location_url(location, format: :json)
end
