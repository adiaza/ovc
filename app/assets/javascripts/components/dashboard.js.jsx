var Dashboard = React.createClass({
  displayName: 'Dashboard Component',

  getDefaultProps() {
    return {
    };
  },

  getInitialState() {
    return {
      guests: this.props.guests
    };
  },

  handleChange(guests) {
    this.setState({guests: guests})
  },

  confirmsAmount(str) {
    confirms = this.state.guests.filter(function(g){
      return g.confirmation_status == str; } );

    return confirms.length;
  },


  render: function() {
    return(
      <div>
        <div className="row">
          <Widget icon="calendar"
                  text={I18n.t('dashboard.widgets.days_left')}
                  color="blue"
                  number={this.props.event.daysLeft}
                  progress={this.props.event.ongoing_days_percent}
                  mdCol="6"
                  xsCol="12" />

          <Widget icon="users"
                  text={I18n.t('dashboard.widgets.total_guests')}
                  color="blue"
                  number={this.state.guests.length}
                  progress="100"
                  mdCol="6" />

          <Widget icon="thumbs-o-up"
                  text={I18n.t('dashboard.widgets.ok_guests')}
                  color="green"
                  number={this.confirmsAmount('ok')}
                  progress={this.props.event.ongoing_days_percent}
                  mdCol="4" />

          <Widget icon="question-circle"
                  text={I18n.t('dashboard.widgets.unknown_guests')}
                  color="yellow"
                  number={this.confirmsAmount('unknown')}
                  progress={this.props.event.ongoing_days_percent}
                  mdCol="4" />

          <Widget icon="thumbs-o-down"
                  text={I18n.t('dashboard.widgets.nok_guests')}
                  color="red"
                  number={this.confirmsAmount('nok')}
                  progress={this.props.event.ongoing_days_percent}
                  mdCol="4" />
        </div>

        <Guests data={this.props.guests} hc={this.handleChange} />
      </div>
    );
  }
});
