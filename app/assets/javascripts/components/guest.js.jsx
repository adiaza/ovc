var Guest = React.createClass({
  displayName: 'Guest Component',

  getInitialState() {
    return {
      edit: false,
      confirmation_status: this.props.guest.confirmation_status
    };
  },

  clickHandlerConfirmation: function(message, event){
    event.preventDefault();
    var data = {
      confirmation_status: message,
      role: 'guest' };

    $.ajax({
      method: 'PUT',
      url: '/dashboard/users/' + this.props.guest.id,
      dataType: 'JSON',
      data: { user: data },
      success: (data) => {
        this.setState({confirmation_status: message});
        this.props.handleEditGuest(this.props.guest, data);
      },
      error: (xhr, status, err) => {
        console.error(this.props.guest, status, err.toString());
      }
    })
  },

  handleToggle: function(e) {
    e.preventDefault();
    this.setState({edit: !this.state.edit})
  },

  handleEdit: function(e, userData) {
    e.preventDefault()
    var data = {
      name: ReactDOM.findDOMNode(userData.refs.name).value,
      email: ReactDOM.findDOMNode(userData.refs.email).value,
      menu: ReactDOM.findDOMNode(userData.refs.menu).value,
      role: 'guest'
    }

    $.ajax({
      method: 'PUT',
      url: '/dashboard/users/' + this.props.guest.id,
      dataType: 'JSON',
      data: { user: data },
      success: (data) => {
        this.setState({edit: false});
        this.props.handleEditGuest(this.props.guest, data); }
    });
  },

  handleDelete: function(e) {
    e.preventDefault()
    $.ajax({
      method: 'DELETE',
      url: '/dashboard/users/' + this.props.guest.id,
      dataType: 'JSON',
      success: () => {
        this.props.handleDeleteGuest(this.props.guest); }
    });
  },

  render: function() {
    if (this.state.edit)
      return ( <GuestEdit guest={this.props.guest}
                          onToggle={this.handleToggle}
                          confirmStatus={this.state.confirmation_status}
                          clickHandler={this.clickHandlerConfirmation}
                          handleEdit={this.handleEdit} /> )
    else
      return ( <GuestRow guest={this.props.guest}
                         onToggle={this.handleToggle}
                         confirmStatus={this.state.confirmation_status}
                         clickHandler={this.clickHandlerConfirmation}
                         handleDelete={this.handleDelete} /> );
    }

});

