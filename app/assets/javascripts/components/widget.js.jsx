var Widget = React.createClass({
    displayName: 'Widget Comp',

    getDefaultProps() {
      return {
        mdCol: 4,
        xsCol: 6
      };
    },


    render: function(){
        return(
          <div className={'col-xs-' + this.props.xsCol +
                         ' col-md-' + this.props.mdCol }>
            <div className={'info-box bg-' + this.props.color }>
              <span className="info-box-icon">
                <i className={'fa fa-' + this.props.icon }></i>
              </span>
              <div className="info-box-content">
                <span className="info-box-text">{this.props.text}</span>
                <span className="info-box-number">{this.props.number}</span>
                <div className="progress">
                  <div className="progress-bar"
                  style={{'width': this.props.progress + '%'  }}>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
    }
});
