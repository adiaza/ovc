var GuestUserForm = React.createClass({
  displayName: 'Guest User Form',

  getInitialState() {
    return {
      name: '',
      email: '',
      menu: '',
      role: 'guest',
      has_partner: false,
      menu_ro: true,
      menu_checkbox: false,
      partner_name: ''
    };
  },

  valid() {
    return this.state.email && this.state.name;
  },

  handleCheckboxChange: function(e) {
    name = e.target.name
    this.setState({ [name]: e.target.checked})
  },

  toggleMenu: function(e) {
    this.setState({menu_ro: !this.state.menu_ro});
    this.setState({menu_checkbox: e.target.name.value});
  },

  handleChange: function(e) {
      name = e.target.name
      this.setState ({[name]: e.target.value})
  },

  handleSubmit: function(e) {
      e.preventDefault();
      $.ajax({
        url: '/dashboard/users',
        dataType: 'json',
        type: 'POST',
        data: { user: this.state },
        success: (d) => {
          if(d.partner)
            this.props.handleNewRecord(d.partner);
          this.props.handleNewRecord(d.user);
          this.setState(this.getInitialState());
        }
      })
    },

  render: function() {
    return(
      <form className="form-inline" onSubmit={this.handleSubmit} >
        <div className="row">
          <div className="form-group form-group-sm">
            <label htmlFor="guest_name">{I18n.t('guestform.name')} </label>
            <input type='text'
                   className="form-control"
                   placeholder={I18n.t('guestform.name')}
                   name="name"
                   id="guest_name"
                   value={this.state.name}
                   onChange={this.handleChange} />
          </div>

          <div className="form-group form-group-sm">
            <label htmlFor="guest_email">{I18n.t('guestform.email')}</label>
            <input type='text' className="form-control"
              placeholder="Email" name="email"
              id= "guest_email" value={this.state.email}
              onChange={this.handleChange} />

            <input type="hidden"
                   className="form-control"
                   name="role"
                   value="guest" />
          </div>

          <div className="checkbox">
            <label htmlFor="guest_menu_checkbox">
              <input type="checkbox" name="guest_menu_checkbox"
                     id="guest_menu_checkbox" checked={this.state.menu_checkbox}
                     onChange={this.toggleMenu}
              />
              {' ' + I18n.t('guestform.menu_label')}
            </label>
          </div>

          <div className="form-group form-group-sm">
            <label className="sr-only" htmlFor="guest_menu"></label>
            <input type='text'
                   className="form-control"
                   placeholder={I18n.t('guestform.menu_ph')}
                   name="menu"
                   id="guest_menu"
                   value={this.state.menu}
                   readOnly={this.state.menu_ro}
                   onChange={this.handleChange} />
          </div>
        </div>

        <div className="row">
          <div className="checkbox">
            <label htmlFor="has_partner">
              <input type="checkbox" id="has_partner" name="has_partner"
                     onChange={this.handleCheckboxChange}
                     checked={this.state.has_partner} />
              {' ' + I18n.t('guestform.partner')}
            </label>
          </div>
        </div>

        {this.state.has_partner ? <PartnerForm hC={this.handleChange }/>
        : null }

        <button className="btn btn-primary btn-sm" type="submit"
          disabled={!this.valid()} >
          <i className="fa fa-save"></i>
          Guardar
        </button>

      </form>
    );
  }

});