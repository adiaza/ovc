var GuestEdit = React.createClass({

  confirmClassName(icon) {
    cName = 'fa ';
    if (icon == 'ok')
      cName = cName + 'fa-thumbs-o-up'
    else if (icon == 'nok')
      cName = cName + 'fa-thumbs-o-down'
    else if (icon == 'unknown')
      cName = cName + 'fa-question'

    if (icon == this.props.confirmStatus)
      cName = cName + ' active '
    else
      cName = cName + ' inactive '

    return cName
  },

  menuDisplay() {
    if(this.props.guest.menu)
      return(
        <a data-toggle = 'tooltip' data-placement='top'
           className='guest_menu_link'
           title={this.props.guest.menu}>Especial</a> );
    else return('Normal');
  },

  render() {
    return (
      <tr>
        <td>
          <input className = 'form-control input-sm' type = 'text'
            defaultValue = {this.props.guest.name}  ref = 'name' />
        </td>

        <td>
          <div className="btn-group">
            <a href="#" className="btn"
               onClick={e => this.props.clickHandler('ok', e) }>
              <i className={this.confirmClassName('ok')}></i> </a>

            <a href="#" className="btn"
               onClick={e => this.props.clickHandler('unknown', e) }>
              <i className={this.confirmClassName('unknown')}></i> </a>

            <a href="#" className="btn"
               onClick={e => this.props.clickHandler('nok', e) }>
              <i className={this.confirmClassName('nok')}></i> </a>
          </div>
        </td>

        <td> <input className = 'form-control input-sm' type = 'text'
            defaultValue = {this.props.guest.email}  ref = 'email' /> </td>

        <td>{this.props.guest.partner && this.props.guest.partner.name}</td>

        <td> <input className = 'form-control input-sm' type = 'text'
            defaultValue = {this.props.guest.menu}  ref = 'menu' /></td>

        <td> <a className="btn" onClick={e => this.props.handleEdit(e, this)}>
            <i className="fa fa-check"></i> </a> </td>

        <td> <a className="btn" onClick={e => this.props.onToggle(e)}>
            <i className="fa fa-undo"></i> </a> </td>

      </tr>
    );
  }
});