R = React.DOM
@PartnerForm = React.createClass
  displayName: 'Partner Form'

  getInitialState: ->
    role: 'partner'
    partner_menu: true


  toggleMenu: (e) ->
    @setState partner_menu: !@state.partner_menu

  render: ->
    R.div
      className: 'row'
      R.div
        className: 'form-group form-group-sm'

        R.label
          htmlFor: 'partner_name'
          I18n.t('guestform.name')

        R.input
          type:      'text'
          className: 'form-control'
          id:        'partner_name'
          name:      'partner_name'
          placeholder:  I18n.t('guestform.name')
          onChange: @props.hC


      R.div
        className: 'form-group form-group-sm'

        R.label
          htmlFor: 'partner_email'
          I18n.t('guestform.email')

        R.input
          type:        'text'
          className:   'form-control'
          placeholder: 'Email'
          name:        'partner_email'
          id:          'partner_email'
          onChange: @props.hC

      R.div
        className: 'checkbox'
        R.label
          htmlFor: 'partner_food_cb'
          R.input
            type: 'checkbox'
            id: 'partner_food_cb'
            onChange: @toggleMenu
          ' ' + I18n.t('guestform.menu_label')

      R.div
        className: 'form-group form-group-sm'

        R.label
          className: 'sr-only'
          htmlFor: 'partner_menu'

        R.input
          type:        'text'
          className:   'form-control'
          placeholder: I18n.t('guestform.menu_ph')
          name:        'partner_menu'
          id:          'partner_menu'
          readOnly:    @state.partner_menu
          onChange: @props.hC

      R.div
        className: 'row'

        R.small {}, I18n.t('guestform.optional_partner_advice')