var Guests = React.createClass({
  displayName: 'Guests Component',

  getDefaultProps() {
    return {
      guests: []
    };
  },

  getInitialState() {
    return {
      guests: this.props.data,
      searchString: ''
    };
  },

  addGuest(guest) {
    guests = React.addons.update(this.state.guests, { $push: [guest] });
    this.setState({guests: guests});
    this.props.hc(guests);
  },

  deleteGuest(guest) {
    index = this.state.guests.indexOf(guest)
    guests = React.addons.update(this.state.guests, { $splice: [[index, 1]] });
    this.setState({guests: guests})
    this.props.hc(guests);
  },

  updateGuest: function(guest, data) {
    index = this.state.guests.indexOf(guest)
    guests = React.addons.update(this.state.guests, { $splice: [[index, 1, data]]})
    this.setState({guests: guests})
    this.props.hc(guests);
  },

  handleChange: function(e) {
    this.setState({searchString:e.target.value});
  },

  render: function() {
    var searchString = this.state.searchString.trim().toLowerCase();
    return(
      <div>
        <h4>Tus Invitados</h4>
        <div className="box">
          <div className="box-header">
            <div className="row guests-toolbar" style={{'lineHeight': '35px'}}>
              <div className="col-md-8">
                <a href="#" id="new_guest_link" className="btn btn-sm btn-default"><i className='fa fa-plus'></i> {I18n.t('dashboard.guests.add_guest')}</a>
                <a href="#" className="btn btn-sm btn-default"><i className='fa fa-send'></i> {I18n.t('dashboard.guests.send_invitation')}</a>
                <a href="#" className="btn btn-sm btn-default"><i className='fa fa-upload'></i> {I18n.t('dashboard.guests.load_bulk')}</a>
                <a href="#" className="btn btn-sm btn-default"><i className='fa fa-download'></i> {I18n.t('dashboard.guests.download_list')}</a>
                <a href="#" className="btn btn-sm btn-danger"><i className='fa fa-trash'></i> {I18n.t('dashboard.guests.delete_all')}</a>
              </div>

              <div className="col-md-4">
                <div className="box-tools pull-right">
                  <div className="input-group input-group-sm" style={{'width': '100%'}} >
                    <input type="text" name="table_search"
                                       value={this.state.searchString}
                                       onChange={this.handleChange}
                                       className="form-control"
                                       style={{'marginTop': '2px'}}
                                       placeholder="Buscar Invitado..." />
                    <div className="input-group-btn">
                      <button type="submit" className="btn btn-default"><i className="fa fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="reactBox">

            <div className="box-header add-guest-form" id="add_guest_form">
              <GuestUserForm handleNewRecord={this.addGuest} />
            </div>

            <div className="box-body table-responsive no-padding">
              <table className="table guests-table table-hover">
                <thead>
                  <tr>
                    <th>Nombre</th>
                    <th>Confirmación</th>
                    <th>Email</th>
                    <th>Pareja</th>
                    <th>Menú</th>
                    <th><i className="fa fa-edit"></i></th>
                    <th><i className="fa fa-trash"></i></th>
                  </tr>
                </thead>

                <tbody>{
                    this.state.guests.map(function(guest){
                      if (guest.name.toLowerCase().match( searchString )){
                      return (<Guest
                                 key={guest.id}
                                 guest={guest}
                                 handleDeleteGuest={this.deleteGuest}
                                 handleEditGuest={this.updateGuest}
                              /> );
                    }
                    }, this)
               }</tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    );
  }
});
