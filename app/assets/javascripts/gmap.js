var church = false;
var church_marker;

function initMap() {
  google.maps.event.addDomListener(window, 'load', initialize);
  google.maps.event.addDomListener(window, 'page:load', initialize);
}


function initialize() {

  if(document.getElementById("submit-button"))
    drag = true
  else
    drag = false

  var location_church_latitude = document.getElementById("location_church_latitude");
  var location_church_longitude = document.getElementById("location_church_longitude");

  var location_longitude = document.getElementById("location_longitude");
  var location_latitude = document.getElementById("location_latitude");

  var location_church_name = document.getElementById("location_church_name");

  if ( location_longitude && location_latitude ) {

    var f_long = parseFloat(location_longitude.value);
    var f_lat = parseFloat(location_latitude.value);

    var myLatLng = {lat: f_lat, lng: f_long };
    var map = new google.maps.Map(document.getElementById('map-canvas'), {
      center: myLatLng,
      zoom: 12,
      scrollwheel:  false

    });

    var marker = new google.maps.Marker({
      position: myLatLng,
      draggable: drag,
      animation: google.maps.Animation.DROP,
      map: map,
      title: 'Hello World!'
    });

    google.maps.event.addListener(marker, 'dragend', function (event) {
      document.getElementById("location_latitude").value = this.getPosition().lat();
      document.getElementById("location_longitude").value = this.getPosition().lng();
      map.panTo(marker.getPosition());
    });


  }

  if (location_church_latitude && location_church_longitude)
  {
    c_long = parseFloat(location_church_longitude.value);
    c_lat = parseFloat(location_church_latitude.value);

    if ( (c_long < 0 || c_long > 0 ) && (c_lat < 0 || c_lat > 0 ) )
    {
      church = true;
      var cLatLng = {lat: c_lat, lng: c_long };
      church_marker = new google.maps.Marker({
        position: cLatLng,
        draggable: drag,
        animation: google.maps.Animation.DROP,
        map: map,
        title: 'Church!',
        icon: 'http://maps.google.com/mapfiles/kml/shapes/church.png'
      });

      google.maps.event.addListener(church_marker, 'dragend', function (event) {
        location_church_latitude.value = this.getPosition().lat();
        location_church_longitude.value = this.getPosition().lng();
        map.panTo(church_marker.getPosition());
      });
    }


    // Only listen for change if field is available
    if (location_church_name){
      google.maps.event.addDomListener(location_church_name, 'change',
        function(){ addChurchMarker(map, location_church_latitude, location_church_longitude); } );
    }

  }

}

function addChurchMarker(map, location_church_latitude, location_church_longitude) {

  var name_value = document.getElementById("location_church_name").value;

  if (location_church_latitude.value == "") { church = false; }

  if (name_value.length > 0 && !church) {

    church = true;

    var f_long = parseFloat(document.getElementById("location_longitude").value);
    var f_lat = parseFloat(document.getElementById("location_latitude").value);

    location_church_latitude.value = f_long;
    location_church_longitude.value = f_lat;

    var myLatLng = {lat: f_lat, lng: f_long };

    church_marker = new google.maps.Marker({
      position: myLatLng,
      draggable: true,
      animation: google.maps.Animation.DROP,
      map: map,
      title: 'church',
      icon: 'http://maps.google.com/mapfiles/kml/shapes/church.png'
    });

    google.maps.event.addListener(church_marker, 'dragend', function (event) {
      location_church_latitude.value = this.getPosition().lat();
      location_church_longitude.value = this.getPosition().lng();
      map.panTo(church_marker.getPosition());
    });

  }
  else if (name_value.length == 0) {
    church_marker.setMap(null);
    church = false;
    location_church_latitude.value = "";
    location_church_longitude.value = "";
  }

}
