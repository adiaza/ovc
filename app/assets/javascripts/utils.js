$(function() {
  $('a#new_guest_link').click(function(event){
    event.preventDefault();
    $('div#add_guest_form').toggle();
  });
});

renderImage = function(fileField) {
    var reader;
    reader = new FileReader;
    reader.onload = function(event) {
      var store = document.getElementById('imgpreview');
      var theUrl;
      theUrl = event.target.result;
      store.innerHTML='<img class="preview" src="' + theUrl +'">';
      // return $(fileField).closest('.sortable').find('.rightpane img').attr('src', theUrl);
    };
    return reader.readAsDataURL(fileField.files[0]);
};

$(document).on('upload:start', 'form', function(e) {
  $(this).find('input[type=submit]').attr('disabled', true);

  progressBar = $('#' + $(e.target).data('progressbar')).parent();
  progressBar.removeClass('hidden');
  progressBar.addClass('show');
});

$(document).on('upload:progress', 'form', function(e) {
  // Get the progress bar to modify
  progressBar = $('#' + $(e.target).data('progressbar'));

  // Process upload details to get the percentage complete
  uploadDetail = e.originalEvent.detail;
  percentLoaded = uploadDetail.progress.loaded;
  totalSize = uploadDetail.progress.total;
  percentageComplete = Math.round((percentLoaded / totalSize) * 100);

  // Reflect the percentage on the progress bar
  progressBar.css('width', percentageComplete + '%');
  progressBar.text(percentageComplete + '%');
});

$(document).on('upload:success', 'form', function(e) {
  progressBar = $('#' + $(e.target).data('progressbar'));
  progressBar.addClass('progress-bar-success');
  renderImage(e.target)
});

$(document).on('upload:failure', 'form', function(e) {
  progressBar = $('#' + $(e.target).data('progressbar'));
  progressBar.addClass('progress-bar-danger');
});

$(document).on('upload:complete', 'form', function(e) {
  if(!$(this).find('input.uploading').length) {
    $(this).find('input[type=submit]').removeAttr('disabled');
  }
});