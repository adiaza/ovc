class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities

    user ||= User.new # guest user (not logged in)


    # Only Admins can manage Dashboard
    # can :manage, :dashboard if user.has_role? :admin

    # Only Admins can manage Users
    can :manage, :all if user.has_role? :admin
    # can    :manage, User       if user.has_role? :admin
    # can    :manage, Location   if user.has_role? :admin
    # can    :manage, Story      if user.has_role? :admin

    cannot :edit, User do |u|
        !u.is_groom_of?(u.event) and !u.is_bride_of?(u.event)
    end

    # Only Admins can manage events


    # can     :manage, Event      if user.has_role? :admin
    cannot  :create, Event      if user.has_role? :admin and user.event_id.present?

    cannot :destroy, Event do |e|
        e != user.event
    end


    # Anyone can read events
    can :read,   Event
  end
end
