class Event < ActiveRecord::Base
  # rolify helper
  resourcify

  has_many :users
  has_many :stories
  has_one :location
  has_one :gift

  before_destroy :unassign_admin
  before_destroy :destroy_guests

  before_create :create_bride
  before_create :create_groom


  def days_left
    (date.to_date - Time.now.to_date).to_i
  end

  def days_left_since_created
    (date.to_date - created_at.to_date).to_i
  end

  def ongoing_days
    days_left_since_created - days_left
  end

  def total_guests
    users.with_role(:guest, self).count
  end

  def confirmed_guests
    users.with_role(:guest, self).ok.count
  end

  def unconfirmed_guests
    users.with_role(:guest, self).nok.count
  end

  def unknown_guests
    users.with_role(:guest, self).unknown.count
  end

  def ongoing_days_percent
    ((ongoing_days*100)/days_left_since_created).to_i
  end

  def as_json(options={})
    h = super(options)
    h[:daysLeft] = days_left
    h[:ongoing_days_percent] = ongoing_days_percent
    h[:total_guests] = total_guests
    h[:confirmed_guests] = confirmed_guests
    h[:unconfirmed_guests] = unconfirmed_guests
    h[:unknown_guests] = unknown_guests
    h
  end


  private

  def unassign_admin
    users.with_role(:admin).update_all(event_id: nil)
  end

  def destroy_guests
    users.destroy_all
  end

  def create_bride
    users.build(password: '12', roles: [ Role.new(name: 'bride', resource: self ) ] )
  end

  def create_groom
    users.build(password: '12', roles: [ Role.new(name: 'groom', resource: self ) ] )
  end

end
