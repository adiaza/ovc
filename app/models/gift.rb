class Gift < ActiveRecord::Base
  belongs_to :event
  belongs_to :store

  validates_presence_of :code
  validates_presence_of :store
end
