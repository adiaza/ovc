class User < ActiveRecord::Base
  enum confirmation_status: [:unknown, :nok, :ok]

  rolify
  # Include default devise modules. Others available are:
  #  :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  attachment :profile_image, type: :image

  belongs_to :event


  has_one :partner, class_name: 'User', foreign_key: :partner_id,
          dependent: :nullify

  after_create :set_partnership


  validates :roles, :presence => true

  def full_name
    "#{first_name} #{last_name}"
  end

  def create_account
      account = Account.new(:email => email, :subdomain => subdomain )
      account.save!
  end
  
  def as_json(options={})
    super(:include => {
            :partner => {:only => [:name]}
          }
    )
  end

  private

  def set_partnership
    unless self.partner_id.nil?
       u = User.find(self.partner_id)
       u.partner_id = self.id
       u.save
    end
  end

  def email_required?
    if self.is_admin? then
      true
    else
      false
    end
  end

end
