class Location < ActiveRecord::Base
  belongs_to :event

  attachment :location_image, type: :image
  attachment :map_image, type: :image

  validates_presence_of :name

end
