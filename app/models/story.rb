class Story < ActiveRecord::Base
  belongs_to :event
  
  attachment :photo
  
  validates_presence_of :name
end
