require 'test_helper'

class EventTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup


  end

  test "When an event is created, it should create a groom" do
    event = Event.new(description: 'meh')
    event.save!

    assert_equal 1, event.users.with_role(:groom, event).count, 'Groom not created'

    assert_equal 1, event.users.with_role(:bride, event).count, 'Bride not created'


  end

end
