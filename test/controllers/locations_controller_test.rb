require 'test_helper'

class LocationsControllerTest < ActionController::TestCase
  setup do
    @location = locations(:one)
    @event = events(:one)

    @admin_with_event = users(:admin_with_event)
    @admin_with_event.add_role :admin
    sign_in @admin_with_event
  end

  test "should get index" do
    get :index, event_id: @event
    assert_response :success
    assert_not_nil assigns(:location)
  end

  test "should get new" do
    get :new, event_id: @event
    assert_response :success
  end

  test "should create location" do
    assert_difference('Location.count') do
      post :create, location: {
         address:           @location.address,
         description:       @location.description,
         event_id:          @location.event_id,
         latitude:          @location.latitude,
         location_image_id: @location.location_image_id,
         longitude:         @location.longitude,
         map_image_id:      @location.map_image_id,
         name:              @location.name },
       event_id:          @event
    end

    assert_redirected_to event_location_path(@event, assigns(:location))
  end

  test "should show location" do
    get :show, id: @location, event_id: @event
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @location, event_id: @event
    assert_response :success
  end

  test "should update location" do
    patch :update, id: @location, location: {
      address:           @location.address,
      description:       @location.description,
      event_id:          @location.event_id,
      latitude:          @location.latitude,
      location_image_id: @location.location_image_id,
      longitude:         @location.longitude,
      map_image_id:      @location.map_image_id,
      name:              @location.name },
    event_id: @event

    assert_redirected_to event_location_path(@event, assigns(:location))
  end

  test "should destroy location" do
    assert_difference('Location.count', -1) do
      delete :destroy, id: @location, event_id: @event
    end

    assert_redirected_to dashboard_path
  end
end
