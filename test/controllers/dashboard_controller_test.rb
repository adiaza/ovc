require 'test_helper'
include Devise::TestHelpers

class DashboardControllerTest < ActionController::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
    @request.env["devise.mapping"] = Devise.mappings[:admin]

    @admin = users(:admin)
    @guest = users(:guest)


  end

  test "should deny access showing a flash message when user doesn't have Admin role" do
    sign_in @guest
    get :index
    assert_not flash.empty?
  end

end
