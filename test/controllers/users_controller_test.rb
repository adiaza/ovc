require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  # test "the truth" do
  #   assert true
  # end

  setup do
    @event = events(:one)

    @admin_with_event = users(:admin_with_event)
    @admin_with_event.add_role :admin
    sign_in @admin_with_event
  end

  test "should add guest and partner to event" do
    assert_difference('User.count', 2) do
      post :create, user: { name: "principal",
                            role: "guest",
                            has_partner: "true",
                            partner_name: "mi partner" }, format: :json
    end
  end

end
