require 'test_helper'


class RegistrationsControllerTest < ActionController::TestCase
  # test "the truth" do
  #   assert true
  # end

  setup do
      @admin = users(:admin)
      @request.env["devise.mapping"] = Devise.mappings[:user]
  end

  test "should redirect to user own domain after sign up" do

  end

  test "should create account after user sign up" do

    assert_difference('Account.count') do
      post :create, user: { email: @admin.email, password: '12', subdomain: 'otro'  }
    end

  end

end
