require 'test_helper'


class EventsControllerTest < ActionController::TestCase
  setup do
    @event = events(:one)
    @event_two = events(:two)

    @guest_user = users(:guest)
    @guest_user.add_role :guest

    @guest_user_two = users(:guesttwo)
    @guest_user_two.add_role :guest

    @admin_user = users(:admin)
    @admin_user.add_role :admin

    @admin_with_event = users(:admin_with_event)
    @admin_with_event.add_role :admin

    users(:guest3).add_role :guest
    users(:guest4).add_role :guest

  end

  test "should not destroy event if not owner" do
    sign_in @admin_with_event

    assert_no_difference('Event.count') do
      delete :destroy, id: @event
    end

  end

  test "event destroy should delete guests" do
    sign_in @admin_with_event
    assert_difference('User.with_role(:guest).count', -2) do
      delete :destroy, id: @event_two.id
    end
  end

  test "event creator should have event assigned" do
    sign_in @admin_user
    post :create, event: { date: @event.date, description: @event.description, latitude: @event.latitude, longitude: @event.longitude, time: @event.time }

    assert_equal assigns(:event).id, @admin_user.reload.event_id

  end

  test "should get index" do
    sign_in @guest_user
    get :index
    assert_response :success
    assert_not_nil assigns(:events)
  end

  test "should get new" do
    sign_in @admin_user
    get :new
    assert_response :success
  end

  test "should create event if admin" do
    sign_in @admin_user

    assert_difference('Event.count') do
      post :create, event: { date: @event.date, description: @event.description, latitude: @event.latitude, longitude: @event.longitude, time: @event.time }
    end

    assert_redirected_to dashboard_path
  end

  test "should show event" do
    sign_in @admin_user
    get :show, id: @event
    assert_response :success
  end

  test "should deny edit if not admin" do
    get :edit, id: @event
    assert_redirected_to new_user_session_path
  end

  test "should update event if admin" do
    sign_in @admin_user

    patch :update, id: @event, event: { date: @event.date, description: @event.description, latitude: @event.latitude, longitude: @event.longitude, time: @event.time }
    assert_redirected_to event_path(assigns(:event))
  end

  test "should destroy event" do
    sign_in @admin_with_event

    assert_difference('Event.count', -1) do
      delete :destroy, id: @event_two
    end

    assert_redirected_to events_path
  end

  test "can't create an event if one is already created" do
    sign_in @admin_user

    post :create, event: { date: @event.date, description: @event.description, latitude: @event.latitude, longitude: @event.longitude, time: @event.time }
    @admin_user.reload
    ability = Ability.new(@admin_user)
    assert_not ability.can?(:create, Event.new(description: "test"))
  end


end
