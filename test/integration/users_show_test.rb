require 'test_helper'
class UsersShowTest  < ActionDispatch::IntegrationTest
  def setup
    host! "testsubdomain.lvh.me:2000"
    @bride = users(:bride)
    @admin = users(:admin_with_event)

    @admin.add_role :admin
    @bride.add_role(:bride, @bride.event)

  end

  test "bride profile should display social network link or string if not present" do

    post user_session_path,  'user[email]' => @admin.email, 'user[password]' => '12'
    follow_redirect!
    get user_path(@bride)
    assert_response :success, @response.body

    assert_select "a[href=?]", @bride.twitter

    assert_select 'div.facebook', I18n.t('no_info')
  end

end