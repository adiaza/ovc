ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require "minitest/reporters"
Minitest::Reporters.use!

Apartment::Tenant.drop('testsubdomain') rescue nil
Apartment::Tenant.create('testsubdomain') rescue nil
Apartment::Tenant.switch!('testsubdomain') rescue nil

# User.create!(email: 'test@lvh.me', subdomain: 'testsubdomain', password: '123456')

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
end

class ActionController::TestCase
  include Devise::TestHelpers
end