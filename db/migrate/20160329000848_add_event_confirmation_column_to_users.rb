class AddEventConfirmationColumnToUsers < ActiveRecord::Migration
  def change
    add_column :users, :confirmation_status, :integer, default: 0
  end
end
