class RemoveAddressFromEvent < ActiveRecord::Migration
  def change
    remove_column :events, :address, :string
    remove_column :events, :address_description, :string
    remove_column :events, :map_image_id, :string
    remove_column :events, :location_image_id, :string

  end
end
