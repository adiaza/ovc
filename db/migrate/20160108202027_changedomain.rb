class Changedomain < ActiveRecord::Migration
  def change
    rename_column :accounts, :domain, :subdomain
  end
end
