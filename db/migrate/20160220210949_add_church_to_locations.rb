class AddChurchToLocations < ActiveRecord::Migration
  def change
    add_column :locations, :church_name, :string
    add_column :locations, :church_address, :string
    add_column :locations, :church_latitude, :float
    add_column :locations, :church_longitude, :float
  end
end
