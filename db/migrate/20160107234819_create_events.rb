class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.datetime :date
      t.time :time
      t.text :description
      t.string :address
      t.text :address_description
      t.decimal :latitude
      t.decimal :longitude

      t.timestamps null: false
    end
  end
end
