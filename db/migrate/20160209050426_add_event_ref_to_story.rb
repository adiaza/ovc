class AddEventRefToStory < ActiveRecord::Migration
  def change
    add_reference :stories, :event, index: true, foreign_key: true
  end
end
