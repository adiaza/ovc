class AddForeignKeyPartnerIdToUsers < ActiveRecord::Migration
  def change
    add_foreign_key :users, :users, column: :partner_id, primary_key: "id"
  end
end
