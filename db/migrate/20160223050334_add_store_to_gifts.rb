class AddStoreToGifts < ActiveRecord::Migration
  def change
    add_reference :gifts, :store, index: true, foreign_key: true
  end
end
