class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :address
      t.text :description
      t.string :location_image_id
      t.string :map_image_id
      t.references :event, index: true, foreign_key: true
      t.string :name
      t.string :lat
      t.string :long

      t.timestamps null: false
    end
  end
end
