# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160401013844) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.string   "email"
    t.string   "subdomain"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "events", force: :cascade do |t|
    t.datetime "date"
    t.time     "time"
    t.text     "description"
    t.decimal  "latitude"
    t.decimal  "longitude"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "gifts", force: :cascade do |t|
    t.text     "description"
    t.string   "code"
    t.integer  "event_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "store_id"
  end

  add_index "gifts", ["event_id"], name: "index_gifts_on_event_id", using: :btree
  add_index "gifts", ["store_id"], name: "index_gifts_on_store_id", using: :btree

  create_table "locations", force: :cascade do |t|
    t.string   "address"
    t.text     "description"
    t.string   "location_image_id"
    t.string   "map_image_id"
    t.integer  "event_id"
    t.string   "name"
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.text     "howto"
    t.string   "church_name"
    t.string   "church_address"
    t.float    "church_latitude"
    t.float    "church_longitude"
  end

  add_index "locations", ["event_id"], name: "index_locations_on_event_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "stores", force: :cascade do |t|
    t.string   "picture"
    t.string   "name"
    t.string   "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "stories", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.date     "date"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "event_id"
    t.string   "photo_id"
  end

  add_index "stories", ["event_id"], name: "index_stories_on_event_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "subdomain"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "email",                  default: ""
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "event_id"
    t.string   "first_name"
    t.string   "last_name"
    t.text     "biography"
    t.string   "profile_image_id"
    t.string   "facebook"
    t.string   "twitter"
    t.string   "instagram"
    t.string   "linkedin"
    t.string   "website"
    t.string   "pinterest"
    t.string   "menu"
    t.string   "name"
    t.integer  "partner_id"
    t.integer  "confirmation_status",    default: 0
  end

  add_index "users", ["event_id"], name: "index_users_on_event_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

  add_foreign_key "gifts", "events"
  add_foreign_key "gifts", "stores"
  add_foreign_key "locations", "events"
  add_foreign_key "stories", "events"
  add_foreign_key "users", "events"
  add_foreign_key "users", "users", column: "partner_id"
end
