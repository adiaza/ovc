# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

s1 = Store.create( [ { name: "Almacenes Paris", picture: "novios_paris.png"  },
                     { name: "Falabella", picture: "novios_falabella.jpg" },
                     { name: "Ripley", picture: "novios_ripley.png"    }
                   ]
                  )