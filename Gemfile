source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.1'
# Use sqlite3 as the database for Active Record
gem 'sqlite3'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc


##
## CUSTOM GEMS
##

# From rails-assets.org
# First, make sure you use bundler >= 1.8.4
gem 'bundler', '>= 1.8.4'

# Postgresql support
gem 'pg'

# This one is for printing nice in the console
gem 'awesome_print'

# For authentication
gem 'devise'

# Bootstrap support
gem 'bootstrap-sass', '~> 3.3.6'

# Very simple Roles library without any authorization enforcement supporting scope on resource object.
# Roles support
gem "rolify"

# CanCan is an authorization library for Ruby on Rails which restricts
# what resources a given user is allowed to access.
gem 'cancancan', '~> 1.10'

#Rails forms made easy.
gem 'simple_form'

# Apartment provides tools to help you deal with multiple tenants in your Rails application.
gem 'apartment'

source 'https://rails-assets.org' do
  gem 'rails-assets-adminlte'
end

# 'font-awesome-sass' is a Sass-powered version of FontAwesome
gem 'font-awesome-sass', '~> 4.5.0'

# provides an easy-to-use and extensible framework for translating your application
gem 'rails-i18n', '~> 4.0.0'

# Ruby gem for automatically transforming JSX and using React in Rails.
gem 'react-rails', '~> 1.5.0'

# Refile is a modern file upload library for Ruby applications. It is simple, yet powerful.
gem "refile", require: "refile/rails"
gem "refile-mini_magick"

# jQuery plugin for drop-in fix binded events problem caused by Turbolinks
gem 'jquery-turbolinks'

# Complete Ruby geocoding solution. http://www.rubygeocoder.com
gem 'geocoder'

# It's a small library to provide the I18n translations on the Javascript. It comes with Rails support.
gem "i18n-js", ">= 3.0.0.rc11"

# Amazon S3 backend for the Refile gem.
gem "refile-s3"

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'

  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'

  ##
  ## CUSTOM GEMS
  ##

  # Guard automates various tasks by running custom rules whenever file or directories are modified.
  gem 'guard'

  # Guard Plugin -  Guard::Minitest automatically run your tests (much like autotest)
  gem 'guard-minitest'

  # create customizable Minitest output formats
  gem 'minitest-reporters'

  # ommit back trace in test outputs
  gem 'mini_backtrace'

  # An IRB alternative and runtime developer console
  gem 'pry'

end

group :development do
  gem 'guard-livereload', '~> 2.4', require: false
  gem 'letter_opener'
end

